# Practical Task: MongoDB

Author: [Ziyodulla Baykhanov](https://www.linkedin.com/in/ziyodulla-baykhanov/)

Date: 5th of May, 2024

Email: [ziyodulla_baykhanov@student.itpu.uz](mailto:ziyodulla_baykhanov@student.itpu.uz)

## Installation and setting up the environment

### Creating a virtual Docker network

Run command in terminal to create a virtual Docker network:

```bash
docker network create bigdata_network
```

![image](screenshots/Screenshot01.png)

Inspect the network to find the gateway's IP address and mark the IP address, which in my case is `172.18.0.1`:

```bash
docker network inspect bigdata_network
```

![image](screenshots/Screenshot02.png)

### Creating the MongoDB Docker container

To install the Docker container for the MongoDB practice, run the following command:

```bash
docker create --name mongo --network bigdata_network -e ADVERTISED_HOST=172.18.0.1 -it mongo
```

![image](screenshots/Screenshot03.png)

### Starting the MongoDB Docker container

Start the Docker container using the following command:

```bash
docker start mongo
```

Verify the "MongoDB" environment is up and running:

```bash
docker ps -a
```

![image](screenshots/Screenshot04.png)

Open a BASH session to the practice environment.

```bash
docker exec -it mongo /bin/bash
ls -l
```

![image](screenshots/Screenshot05.png)

## 1. General details and practice preparation

1. Copy sourse data `products.json` to the container:

    ```bash
    docker cp C:/Users/ziyod/Downloads/products.json mongo:/data/products.json
    ```

    ![image](screenshots/Screenshot06.png)

2. Verify that the file is copied to the Docker container. Provide the command you used for this:

    ```bash
    cd /data
    ls -l
    ```

    ![image](screenshots/Screenshot07.png)

## 2. Import products data into MongoDB

Import the products information from the JSON file you have loaded into MongoDB. See the guidelines if you require assistance on this.

1. Specify an option so that the collection will be dropped if it exists before loading the new data. Provide the code you used for this.

    ```bash
    mongosh --port 27017 --eval "db.products.drop()" epam
    ```

    ![image](screenshots/Screenshot08.png)

2. Import to the collection named `products` and a database name `epam`. Specify the default MongoDB port in the relevant parameter. Provide the code you used for this.

    ```bash
    mongoimport --port 27017 --db epam --collection products --file /data/products.json --drop
    ```

    ![image](screenshots/Screenshot09.png)

## 3. Verify the loaded data in MongoDB

1. Login to MongoDB.

    1.1. Provide the answers to these questions: "Do we have to specify the hostname and port number? Why?"

    **Answer**: I don't necessarily need to specify the hostname and port number if MongoDB is running on the default host (`localhost`) and port (`27017`). However, if MongoDB is running on a different host or port, I need to specify them to connect.

    It’s necessary to specify hostname when connecting to a MongoDB instance that is not running on my local machine. Port Number is required when the MongoDB instance is not listening on the default port (`27017`).

    1.2. Write a command to see the MongoDB version. Execute the command. Provide both the command and the execution results.

    ```bash
    mongod --version
    mongosh --eval "db.version()"
    ```

    ![image](screenshots/Screenshot10.png)

2. Check what options are available in MongoDB for the following, provide the code you used for this and the execution results.

    2.1. Databases.

    ```bash
    mongosh --eval "db.adminCommand('listDatabases')"
    ```

    This command lists all databases available in the MongoDB instance.

    ![image](screenshots/Screenshot11.png)

    2.2. Collection.

    ```bash
    mongosh --eval "db.getCollectionInfos()"
    ```
    
    This command lists all collections in the current database.

    ![image](screenshots/Screenshot12.png)

    2.3. Find options in collections.

    ```bash
    mongosh --eval "db.getCollection('products').find().help()"
    ```

    This command lists all the find options available for the specified collection.

    ![image](screenshots/Screenshot13.png)

3. Check which databases currently exist in this MongoDB instance. Provide both the command and the execution results.

    ```bash
    mongosh --eval "db.adminCommand('listDatabases')"
    ```

    This command lists all databases currently existing in the MongoDB instance.

    ![image](screenshots/Screenshot14.png)

4. Switch to use the database named `epam` and check which collections currently exist in the database `epam`. Provide both the command and the execution results.

    ```bash
    mongosh #Open MongoDB shell
    use epam #Switch to the database named epam
    show collections #List all collections in the current database
    ```

    This command opens MongoDB shell, switches to the database named `epam` and lists all collections in the current database.

    ![image](screenshots/Screenshot15.png)

5. List all data in the collection `products` and check how many documents currently exist in this collection. Provide both the command and the execution results.

    ```bash
    db.products.find()
    db.products.countDocuments()
    ```

    This command lists all data in the collection `products` and counts how many documents currently exist in this collection.

    ![image](screenshots/Screenshot16.png)
    ![image](screenshots/Screenshot17.png)
    ![image](screenshots/Screenshot18.png)

## 4. CRUD operations in MongoDB collections

1. Insert the following new document to the "products" collection with the following attributes. Check that the product was added. Provide both the command and the execution results.

- Product id: "ac9".
- Product name: "AC9 Phone".
- Product brand: "ACME".
- Product type: "phone".
- Product price: 333.
- Product Warranty (in years): 0.25.
- Product availability: true.

```bash
db.products.insertOne({
    _id: "ac9",
    name: "AC9 Phone",
    brand: "ACME",
    type: "phone",
    price: 333,
    warranty_years: 0.25,
    available: true
})
```

To check that the product was added, I can run the following command:

```bash
db.products.find({ _id: "ac9" })
```

![image](screenshots/Screenshot19.png)

2. Perform queries to display products according to the following requirements. Provide both the command and the execution results.

3. Query 1:

- Skip the first 2 products and display the next 10 products in the collection.
- Make the output in an easy-to-read JSON format (each field and its value should appear in a separate row).

```bash
db.products.find().skip(2).limit(10).pretty()

db.products.find().skip(2).limit(10).forEach(printjson)
```

![image](screenshots/Screenshot20.png)

4. Query 2: 

- Display only the "name" and "brand" fields for each product.

```bash
db.products.find({}, { name: 1, brand: 1, _id: 0 })
```

![image](screenshots/Screenshot21.png)

5. Query 3: 

- Display only the "id" and "limits" fields for the first 10 products.
- Collect the results into a single array, in which each element is both "id" and "limits" of a specific product.
- Examine the result you have received, provide the answer to this question: Did all "id" values had a matching "limits" value? Why so?

```bash
db.products.find({}, { _id: 1, limits: 1 }).limit(10).toArray()
```

**Answer**: Not all "id" values had a matching "limits" value. This is because not all products have the “limits” field defined.

![image](screenshots/Screenshot22.png)

6. Query 4: 

- Display the IDs, names, and prices of all products of which prices are greater or equal to 200.

```bash
db.products.find({ price: { $gte: 200 } }, { _id: 1, name: 1, price: 1 })
```

![image](screenshots/Screenshot23.png)

7. Query 5:

- Display the IDs, names, and prices of all products.
- Sort the result according to the price in descending order, and name in ascending order (secondary sort).

```bash
db.products.find({}, { _id: 1, name: 1, price: 1 }).sort({ price: -1, name: 1 })
```

![image](screenshots/Screenshot24.png)

8. Query 6: 

- Write a query that displays how many products we have of type "service" (use the field which is named "type").

```bash
db.products.countDocuments({ type: "service" })
```

![image](screenshots/Screenshot25.png)

9. Updating records. Provide answers to these questions:

- Q1: Can we update the "_id" field? Why so?
- A1: No, we cannot update the "_id" field because it serves as the unique identifier for a document and is immutable once assigned.

- Q2: When should we use the "set" keyword? What happens if we omit it?
- A2: We should use the "$set" keyword when we want to update specific fields in a document. If we omit it, the entire document will be replaced with the new document.

- Q3: When should use the "multi" keyword?
- A3: We should use the "multi" keyword when we want to update multiple documents that match the specified criteria.

10. Perform a query after each of the following updates to verify you have updated the documents as expected. Provide all commands and execution results.

    Answer: Done on the above queries and screenshots.

11. Update 1. Update product with ID "ac3", so that it will now have only the following field values:

- Company: "EPAM".
- Item: "MongoDB".

```bash
# View the current document with the ID "ac3" before updating it:
db.products.find({ _id: "ac3" })

# Update the document with the ID "ac3":
db.products.updateOne({ _id: "ac3" }, { $set: { company: "EPAM", item: "MongoDB" } })

# View the updated document with the ID "ac3":
db.products.find({ _id: "ac3" })
```

![image](screenshots/Screenshot26.png)

12. Update 2. Update all the products which have "ac3" somewhere in their name and add a new "subtype" field to their document with the value "AC3". Note that the "ac3" string in the name can be either lower or upper case.

```bash
# View the current documents with the name containing "ac3" before updating them:
db.products.find({ name: { $regex: /ac3/i } })

# Update the documents with the name containing "ac3":
db.products.updateMany({ name: { $regex: /ac3/i } }, { $set: { subtype: "AC3" } })

# View the updated documents with the name containing "ac3":
db.products.find({ name: { $regex: /ac3/i } })
```

![image](screenshots/Screenshot27.png)
![image](screenshots/Screenshot28.png)


13. Deleting records. Remove all records of type "service" (use the field which is named "type"). Provide the command and execution result, as well as the command and result of checking that the deletion was successful.

```bash
# Check the current documents with the type "service" before deleting them:
db.products.find({ "type": "service" }).count()

# Delete the documents with the type "service":
db.products.deleteMany({ type: "service" })

# Check the current documents with the type "service" after deleting them:
db.products.find({ "type": "service" }).count()
```

![image](screenshots/Screenshot29.png)

## 5. Using indexes

1. Create an index for the "price" field. Provide both the command and the execution results.

    ```bash
    db.products.createIndex({ price: 1 })
    ```

    ![image](screenshots/Screenshot30.png)

2. Create a compound index for "type" and "subtype" fields. Provide both the command and the execution results.

    ```bash
    db.products.createIndex({ type: 1, subtype: 1 })
    ```

    ![image](screenshots/Screenshot31.png)

3. Create a text index for the "name" field. Provide both the command and the execution results.

    ```bash
    db.products.createIndex({ name: "text" })
    ```

    ![image](screenshots/Screenshot32.png)

4. Provide the answer to this question: What is the benefit of a text index over a regular index?

    **Answer**: The main benefit of a text index over a regular index is that a text index supports text search functionality, allowing for efficient searching of text data within MongoDB. Text indexes enable MongoDB to perform full-text search queries, including wildcard searches, phrase searches, and linguistic analysis, which is not possible with regular indexes. Regular indexes are suitable for querying exact matches or range queries on fields like numbers or dates, but they are not optimized for searching text data efficiently. Text indexes, on the other hand, are specifically designed for text search operations, making them more effective for text-based search queries.

## 6. Architecture and monitoring

1. Run a command which describes the current MongoDB node. Provide both the command and the execution results.

    ```bash
    db.runCommand({ "hello": 1 })
    ```

    ![image](screenshots/Screenshot33.png)

2. Change the command to display only the local time of the current instance. Provide both the command and the execution results.

    ```bash
    db.runCommand({ "hello": 1 }).localTime
    db.serverStatus().localTime
    ```

    ![image](screenshots/Screenshot34.png)

3. Run a command which describes the current state of the database, with all its metrics and stats. Provide both the command and the execution results.

    ```bash
    db.stats()
    db.serverStatus()
    ```

    ![image](screenshots/Screenshot35.png)
    ![image](screenshots/Screenshot36.png)

4. Display information about all currently running operations in the database instance. Provide both the command and the execution results.

    ```bash
    db.currentOp()
    ```

    ![image](screenshots/Screenshot37.png)
    ![image](screenshots/Screenshot38.png)

5. Check if replication sets are currently enabled. Provide both the command and the execution results.

    Edit Configuration File: Edit the MongoDB configuration file (`/etc/mongod.conf`) and add the replication configuration options.

    ```bash
    replication:
    replSetName: "myReplicaSet"
    ```

    ![image](screenshots/Screenshot39.png)

    Restart the MongoDB Server:

    ```bash
    docker restart mongo
    ```

    Initiate the Replica Set:
    
    ```bash
    rs.initiate({_id: "myReplicaSet", members: [{_id: 0, host: "localhost:27017"}]})
    ```

    Check the Replica Set Status:

    ```bash
    rs.status()
    ```